// Neil Fisher (1939668)

package movies.importer;

import java.io.IOException;

public class ProcessingTest {
	public static void main(String[] args) throws IOException {
		// LowercaseProcessor
		LowercaseProcessor lowercase = new LowercaseProcessor(
				"D:\\Other files\\Neil's Stuff\\School\\Semester 3\\programming 3\\labs\\fall2020lab05\\eclipse\\src\\input_text_files",
				"D:\\Other files\\Neil's Stuff\\School\\Semester 3\\programming 3\\labs\\fall2020lab05\\eclipse\\src\\output_lowercase_text_files");

		lowercase.execute();

		// RemoveDuplicates
		RemoveDuplicates removeDuplicates = new RemoveDuplicates(
				"D:\\Other files\\Neil's Stuff\\School\\Semester 3\\programming 3\\labs\\fall2020lab05\\eclipse\\src\\output_lowercase_text_files",
				"D:\\Other files\\Neil's Stuff\\School\\Semester 3\\programming 3\\labs\\fall2020lab05\\eclipse\\src\\output_noduplicates_text_files");

		removeDuplicates.execute();
	}
}