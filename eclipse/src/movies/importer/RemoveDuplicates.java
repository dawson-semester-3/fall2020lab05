// Neil Fisher (1939668)

package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor {
	public RemoveDuplicates(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}

	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> noDuplicates = new ArrayList<String>();

		for (int i = 0; i < input.size(); i++) {
			if (!noDuplicates.contains(input.get(i))) {
				noDuplicates.add(input.get(i));
			}
		}

		return noDuplicates;
	}
}
