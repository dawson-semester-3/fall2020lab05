// Neil Fisher (1939668)

package movies.importer;

import java.util.ArrayList;

public class LowercaseProcessor extends Processor {
	public LowercaseProcessor(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}

	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> asLower = new ArrayList<String>();

		for (int i = 0; i < input.size(); i++) {
			asLower.add(input.get(i).toLowerCase());
		}

		return asLower;
	}
}
